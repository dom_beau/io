#include "../include/Lexer.h"

#include "../include/log.h"

#include <regex>

using namespace std;

namespace io
{
    namespace lexer
    {

//------------------------------------------------------------------------------

Lexer::Lexer()
{
    // Build a vector containing lambda functions to call the different token type lexers.
    mLexers.emplace_back( [&]( string::const_iterator& a, string::const_iterator b )-> auto
        { return getStringLiteralToken( a, b ); } );
    mLexers.emplace_back( [&]( string::const_iterator& a, string::const_iterator b )-> auto
        { return getEolToken( a, b ); } );
    mLexers.emplace_back( [&]( string::const_iterator& a, string::const_iterator b )-> auto
        { return getOperatorToken( a, b ); } );
    mLexers.emplace_back( [&]( string::const_iterator& a, string::const_iterator b )-> auto
        { return getConstantToken( a, b ); } );
    mLexers.emplace_back( [&]( string::const_iterator& a, string::const_iterator b )-> auto
        { return getBoolToken( a, b ); } );
    mLexers.emplace_back( [&]( string::const_iterator& a, string::const_iterator b )-> auto
        { return getIdentifierToken( a, b ); } ); 
    mLexers.emplace_back( [&]( string::const_iterator& a, string::const_iterator b )-> auto
        { return getEnclosureToken( a, b ); } ); 
}   

//------------------------------------------------------------------------------

Token Lexer::getNextToken( string::const_iterator& prFirst, const string::const_iterator pLast ) const
{
    LOG_TRACE_IN();
    
    LOG_DEBUG << "-------------";
    LOG_DEBUG << "Input string is : " << string( prFirst, pLast );
    
    Token lToken;
    
    // Try to match a token a the very begining of the range, skipping the leading whitespaces
    //----------------------------------------------------------------------------------------
    while ( prFirst != pLast && (*prFirst == ' ' || *prFirst == '\t' || *prFirst == '\n' || *prFirst == '\r' ) )
    {
        ++prFirst;
    }

    // Skip comments '#...' to the begining of next line
    //--------------------------------------------------
    if ( prFirst != pLast && *prFirst == '#' )
    {
        string::const_iterator lIt = prFirst;
        
        while ( lIt != pLast && *lIt != '\n' && *lIt != '\r' )
        {
            ++lIt;
        }
        
        if ( lIt != pLast )
        {
            lToken = Token( string( prFirst, lIt ), TokenType::COMMENT );
            prFirst = lIt;
        }
        
        LOG_DEBUG << "Found comment: " << lToken.value();
    }
    
    // If prFirst == pLast, that means that we reached the EOF...
    if ( prFirst == pLast )
    {
        lToken = Token( "", TokenType::EOFILE );
    }
    
    if ( lToken.type() == TokenType::UNKNOWN )
    {
        // Call the different lexers in order to lex correctly the string.
        for ( auto lLexer : mLexers )
        {
            if ( (lToken = lLexer( prFirst, pLast )).type() != TokenType::UNKNOWN )
            {
                break;
            }
        }
    }
    
    LOG_TRACE_OUT();
    
    return lToken;
}
      
//------------------------------------------------------------------------------

vector<Token> Lexer::getAllTokens( const string& prStr ) const
{
    LOG_TRACE_IN();
    
    vector<Token> lTokens;
    string::const_iterator lFirst = prStr.cbegin();
    string::const_iterator lLast = prStr.cend();
    
    while ( lFirst != lLast )
    {
        Token lToken = getNextToken( lFirst, lLast );
        
        if ( lToken.type() != TokenType::COMMENT )
        {
            lTokens.push_back( lToken );
        }
        
        if ( lTokens.size() > 0 )
        {
            LOG_DEBUG << lTokens.back().str();
        
            if ( lTokens.back().type() == TokenType::UNKNOWN )
            {
                LOG_DEBUG << "Break on error";
                break;
            }
        }
    }
    
    LOG_TRACE_OUT();
    
    return lTokens;
}
      
//------------------------------------------------------------------------------
    
Token Lexer::getStringLiteralToken( string::const_iterator& prFirst, const string::const_iterator pLast ) const
{
    LOG_TRACE_IN();
    
    LOG_DEBUG << "String to lex for string literal token = " << string( prFirst, pLast );
    
    Token lToken;
    
    if ( *prFirst == '"' )
    {
        // If it is a string literal, search it "by hand"
        string::const_iterator lBegin = prFirst;
        
        do
        {
            ++prFirst;
            
            if ( prFirst == pLast ) break;
            if ( *prFirst == '\"' && *(prFirst - 1) != '\\' ) break;
        }
        while ( true );
    
        if ( prFirst != pLast )
        {
            ++prFirst;           
            
            lToken = Token( string( lBegin, prFirst ), TokenType::STRINGLITERAL );
            
            LOG_DEBUG << "String literal found: " << lToken.value();
        }
        else
        {   
            LOG_DEBUG << "prFirst == pLast while lexing string literal";
        }
    }
    
    LOG_TRACE_OUT();
    
    return lToken;
}

//------------------------------------------------------------------------------
    
Token Lexer::getEolToken( string::const_iterator& prFirst, const string::const_iterator pLast ) const
{
    LOG_TRACE_IN();

    LOG_DEBUG << "String to lex for EOL token = " << string( prFirst, pLast );

    Token lToken;

    if ( prFirst != pLast )
    {
        if ( *prFirst == ';' )
        {
            lToken = Token( string( 1, *prFirst ), TokenType::EOLINE );
        
            ++prFirst;

            LOG_DEBUG << "EOL found: " << lToken.value();
        }
    }
 
    LOG_TRACE_OUT();
    
    return lToken;
}

//------------------------------------------------------------------------------
    
Token Lexer::getOperatorToken( string::const_iterator& prFirst, const string::const_iterator pLast ) const
{
    LOG_TRACE_IN();

    LOG_DEBUG << "String to lex for operator token = " << string( prFirst, pLast );

    Token lToken;

    if ( prFirst != pLast )
    {
        if ( *prFirst == '+' || 
                *prFirst == '-' || 
                *prFirst == '*' || 
                *prFirst == '/' || 
                *prFirst == '=' ||
                *prFirst == ',' )
        {
            lToken = Token( string( 1, *prFirst ), TokenType::OPERATOR );
        
            ++prFirst;
        
            LOG_DEBUG << "Operator found: " << lToken.value();
        }
    }
 
    LOG_TRACE_OUT();
    
    return lToken;    
}

//------------------------------------------------------------------------------
    
Token Lexer::getConstantToken( string::const_iterator& prFirst, const string::const_iterator pLast ) const
{
    LOG_TRACE_IN();
    
    LOG_DEBUG << "String to lex for numerical constant token = " << string( prFirst, pLast );

    Token lToken;
    smatch lMatch;

    if ( prFirst != pLast )
    {
        if ( regex_search( prFirst, pLast, lMatch, regex( "^([0-9]+)" ) ) )
        {
            // Find numerical constant : digits
            lToken = Token( lMatch.str( 1 ), TokenType::CONSTANT );

            prFirst += lMatch.length( 1 );            

            LOG_DEBUG << "Numerical constant found: " << lToken.value();
        }
    }
    
    LOG_TRACE_OUT();
    
    return lToken;
}

//------------------------------------------------------------------------------

Token Lexer::getBoolToken( string::const_iterator& prFirst, const string::const_iterator pLast ) const
{
    LOG_TRACE_IN();
    
    LOG_DEBUG << "String to lex for bool token = " << string( prFirst, pLast );

    Token lToken;
    smatch lMatch;

    if ( prFirst != pLast )
    {
        if ( regex_search( prFirst, pLast, lMatch, regex( "^(true|false)" ) ) )
        {
            // Find bool constant 
            lToken = Token( lMatch.str( 1 ), TokenType::BOOL );

            prFirst += lMatch.length( 1 );            

            LOG_DEBUG << "Bool constant found: " << lToken.value();
        }
    }
    
    LOG_TRACE_OUT();
    
    return lToken;
}

//------------------------------------------------------------------------------

Token Lexer::getIdentifierToken( string::const_iterator& prFirst, const string::const_iterator pLast ) const
{
    LOG_TRACE_IN();
    
    LOG_DEBUG << "String to lex for identifier token = " << string( prFirst, pLast );

    Token lToken;
    smatch lMatch;

    if ( prFirst != pLast )
    {
        if ( regex_search( prFirst, pLast, lMatch, regex( "^(\\w+)" ) ) )
        {
            // Find identifier 
            lToken = Token( lMatch.str( 1 ), TokenType::IDENTIFIER );

            prFirst += lMatch.length( 1 );            

            LOG_DEBUG << "Identifier found: " << lToken.value();
        }
    }
    
    LOG_TRACE_OUT();
    
    return lToken;
}

//------------------------------------------------------------------------------
  
Token Lexer::getEnclosureToken( std::string::const_iterator& prFirst, const std::string::const_iterator pLast ) const
{
    LOG_TRACE_IN();

    LOG_DEBUG << "String to lex for enclosure token = " << string( prFirst, pLast );

    Token lToken;

    if ( prFirst != pLast )
    {
        if ( *prFirst == '(' )
        {
            lToken = Token( string( 1, *prFirst ), TokenType::LPARENT );
        }    
        else if ( *prFirst == ')' )
        {
            lToken = Token( string( 1, *prFirst ), TokenType::RPARENT );
        }
        else if ( *prFirst == '{' )
        {
            lToken = Token( string( 1, *prFirst ), TokenType::LBRACE );
        }
        else if ( *prFirst == '}' )
        {
            lToken = Token( string( 1, *prFirst ), TokenType::RBRACE );
        }
        
        if ( lToken.type() != TokenType::UNKNOWN )
        {
            ++prFirst;
        
            LOG_DEBUG << "Enclosure found: " << lToken.value();
        }
    }
 
    LOG_TRACE_OUT();
    
    return lToken;        
}
  
//------------------------------------------------------------------------------
    
    }
}
