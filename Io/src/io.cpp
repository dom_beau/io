/* 
 * File:   io.cpp
 * Author: dbeauchamp
 *
 * Created on 2 mai 2017, 21:31
 */

#include "../include/log.h"
#include "../include/Lexer.h"

#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

using namespace std;
using namespace io;

/*
 * 
 */
int main( int argc, char** argv )
{   
//    LOG_SEVERITY_TRACE();
    LOG_SEVERITY_DEBUG();
    LOG_TRACE_FUNC();

    // Read the source file
    ifstream lSourceFile( argv[1] );
    string lSourceCode = "";
    string lSourceLine;

    getline( lSourceFile, lSourceLine );
    lSourceCode += lSourceLine + "\n";    
    
    do
    {   
        getline( lSourceFile, lSourceLine );
        lSourceCode += lSourceLine + "\n";
    }
    while ( !lSourceFile.eof() );
        
    lSourceFile.close();
    
    lexer::Lexer lLexer;
    
    vector<lexer::Token> lTokens = lLexer.getAllTokens( lSourceCode );
    
    for ( auto lToken : lTokens )
    {
        cout << "'" << lToken.value() << "', " << lToken.str() << endl;
    }
    
    return 0;
}

