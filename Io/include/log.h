/* 
 * File:   log.h
 * Author: dbeauchamp
 *
 * Created on 1 décembre 2015, 23:57
 */

#ifndef LOG_H
#define	LOG_H

// Uses libboost_log.so
#define BOOST_LOG_DYN_LINK
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>

//------------------------------------------------------------------------------
// These namespaces could be used in the source files...
//
//namespace logging = boost::log;
//namespace sinks = boost::log::sinks;
//namespace src = boost::log::sources;
//namespace expr = boost::log::expressions;
//namespace attrs = boost::log::attributes;
//namespace keywords = boost::log::keywords;

//------------------------------------------------------------------------------
// Trivial logging
//
//    BOOST_LOG_TRIVIAL(trace) << "A trace severity message";
//    BOOST_LOG_TRIVIAL(debug) << "A debug severity message";
//    BOOST_LOG_TRIVIAL(info) << "An informational severity message";
//    BOOST_LOG_TRIVIAL(warning) << "A warning severity message";
//    BOOST_LOG_TRIVIAL(error) << "An error severity message";
//    BOOST_LOG_TRIVIAL(fatal) << "A fatal severity message";

#define LOG_TRACE   BOOST_LOG_TRIVIAL( trace )
#define LOG_DEBUG   BOOST_LOG_TRIVIAL( debug )
#define LOG_INFO    BOOST_LOG_TRIVIAL( info )
#define LOG_WARNING BOOST_LOG_TRIVIAL( warning )
#define LOG_ERROR   BOOST_LOG_TRIVIAL( error )
#define LOG_FATAL   BOOST_LOG_TRIVIAL( fatal )

#define LOG_TRACE_FL   BOOST_LOG_TRIVIAL( trace ) << "[" << __FILE__ << "] [" << __LINE__ << "] "
#define LOG_DEBUG_FL   BOOST_LOG_TRIVIAL( debug ) << "[" << __FILE__ << "] [" << __LINE__ << "] "
#define LOG_INFO_FL    BOOST_LOG_TRIVIAL( info ) << "[" << __FILE__ << "] [" << __LINE__ << "] "
#define LOG_WARNING_FL BOOST_LOG_TRIVIAL( warning ) << "[" << __FILE__ << "] [" << __LINE__ << "] "
#define LOG_ERROR_FL   BOOST_LOG_TRIVIAL( error ) << "[" << __FILE__ << "] [" << __LINE__ << "] "
#define LOG_FATAL_FL   BOOST_LOG_TRIVIAL( fatal ) << "[" << __FILE__ << "] [" << __LINE__ << "] "

// deprecated
#define LOG_TRACE_FUNC() LOG_TRACE << "Entering " << __PRETTY_FUNCTION__ << " at line " << __LINE__ << " in file " << __FILE__;

#define LOG_TRACE_IN() LOG_TRACE << "Entering " << __PRETTY_FUNCTION__ << " at line " << __LINE__ << " in file " << __FILE__;
#define LOG_TRACE_OUT() LOG_TRACE << "Leaving " << __PRETTY_FUNCTION__ << " at line " << __LINE__ << " in file " << __FILE__;

#define LOG_SEVERITY_TRACE()    boost::log::core::get()->set_filter( boost::log::trivial::severity >= boost::log::trivial::trace )
#define LOG_SEVERITY_DEBUG()    boost::log::core::get()->set_filter( boost::log::trivial::severity >= boost::log::trivial::debug )
#define LOG_SEVERITY_INFO()     boost::log::core::get()->set_filter( boost::log::trivial::severity >= boost::log::trivial::info )
#define LOG_SEVERITY_WARNING()  boost::log::core::get()->set_filter( boost::log::trivial::severity >= boost::log::trivial::warning )
#define LOG_SEVERITY_ERROR()    boost::log::core::get()->set_filter( boost::log::trivial::severity >= boost::log::trivial::error )
#define LOG_SEVERITY_FATAL()    boost::log::core::get()->set_filter( boost::log::trivial::severity >= boost::log::trivial::fatal )

#endif	/* LOG_H */

