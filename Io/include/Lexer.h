/* 
 * File:   Lexer.h
 * Author: dbeauchamp
 *
 * Created on 23 mai 2017, 23:16
 */

#ifndef LEXER_H
#define	LEXER_H

#include <functional>
#include <string>
#include <vector>

namespace io
{
    namespace lexer
    {
    
//==============================================================================

/// Type of tokens
enum TokenType
{
    UNKNOWN     = 0,
    COMMENT,            // Comment, discard
    EOLINE,             // End of line (';')
    EOFILE,             // End of file
    OPERATOR,           // Operator      
    STRINGLITERAL,      // String literal    
    IDENTIFIER,         // Identifier (var name, etc.)
    CONSTANT,           // Numerical constant
    LPARENT,            // '('
    RPARENT,            // ')'
    LBRACE,             // '{'
    RBRACE,             // '}'
    BOOL,               // Boolean constant
};
        
//==============================================================================

/// \class Token
/// \brief Represents a token with its value and its type.
class Token
{
public:      
    virtual ~Token(){}
    Token() :
        mValue( "" ),
        mType( TokenType::UNKNOWN )
    {}
    
    Token( Token&& prrArg )
    {
        mValue = prrArg.mValue;
        mType = prrArg.mType;        
    }
    
    Token( const Token& prArg )
    {
        mValue = prArg.mValue;
        mType = prArg.mType;
    }
    
    Token( const std::string& prValue, const TokenType pType ) :
        mValue( prValue),
        mType( pType )
    {}

    Token& operator=( const Token& prArg )
    {
        if ( &prArg != this )
        {
            mValue = prArg.mValue;
            mType = prArg.mType;
        }
        
        return *this;        
    }
    
    Token& operator=( Token&& prrArg )
    {
        if ( &prrArg != this )
        {
            mValue = prrArg.mValue;
            mType = prrArg.mType;
        }
        
        return *this;
    }
    
    const char* value() const { return mValue.c_str(); }
    const TokenType type() const { return mType; }
    
    const char* str() const
    {
        switch ( mType )
        {
            case TokenType::COMMENT:        { return "COMMENT"; }
            case TokenType::EOLINE:         { return "EOLINE"; }
            case TokenType::EOFILE:         { return "EOFILE"; }
            case TokenType::IDENTIFIER:     { return "IDENTIFIER"; }
            case TokenType::CONSTANT:       { return "CONSTANT"; }
            case TokenType::STRINGLITERAL:  { return "STRINGLITERAL"; }
            case TokenType::OPERATOR:       { return "OPERATOR"; }
            case TokenType::BOOL:           { return "BOOL"; }
            case TokenType::LPARENT:        { return "LPARENT"; }
            case TokenType::RPARENT:        { return "RPARENT"; }
            case TokenType::LBRACE:         { return "LBRACE"; }
            case TokenType::RBRACE:         { return "RBRACE"; }
            default:                        { return "UNKNOWN"; }
        }
    }
private:
    std::string mValue;
    TokenType   mType;
};

//==============================================================================

/// \class Lexer
/// \brief Offers lexing facilities. Convert a string into tokens.
class Lexer
{
public:
    ~Lexer(){}
    Lexer();
    
    /// Given string iterators, find the next token.
    ///
    /// \param prFirst is the begining of the string. It is updated to the next
    /// token on return.
    /// \param pLast is the iterator past the last character of the string.
    ///
    /// \return the first token.
    Token getNextToken( std::string::const_iterator& prFirst, const std::string::const_iterator pLast ) const;
    
    /// Given a string, returns a vector of tokens.
    ///
    /// \param prStr is the string to be tokenized.
    ///
    /// \return a vector of all the tokens found in the string.
    std::vector<Token> getAllTokens( const std::string& prStr ) const;
    
private:
    Token getStringLiteralToken( std::string::const_iterator& prFirst, const std::string::const_iterator pLast ) const;
    Token getEolToken( std::string::const_iterator& prFirst, const std::string::const_iterator pLast ) const;
    Token getOperatorToken( std::string::const_iterator& prFirst, const std::string::const_iterator pLast ) const;
    Token getConstantToken( std::string::const_iterator& prFirst, const std::string::const_iterator pLast ) const;
    Token getBoolToken( std::string::const_iterator& prFirst, const std::string::const_iterator pLast ) const;
    Token getIdentifierToken( std::string::const_iterator& prFirst, const std::string::const_iterator pLast ) const;
    Token getEnclosureToken( std::string::const_iterator& prFirst, const std::string::const_iterator pLast ) const;
    Token getCommaToken( std::string::const_iterator& prFirst, const std::string::const_iterator pLast ) const;
    
    typedef std::function<Token( std::string::const_iterator&, std::string::const_iterator )> lexer_signature_t;
    
    std::vector<lexer_signature_t> mLexers;
};

//------------------------------------------------------------------------------

    }
}

#endif	/* LEXER_H */

